import toFileTree, { fileTreeToTreeBeard, toTreeBeard } from '../src/toFileTree'

const paths = [
  "index.html",
  "other_file.html",
  "here/it/is/index.html",
  "here/it/index.html",
  "another/file.html",
  "another/index.html",
  "here/it/is/file.html",
];

/**
 * ToFileTree tests
 */
describe('toFileTree without full file paths', () => {
  const fileTree = toFileTree(paths, false)
  it('top level files are in in top level files', () => {
    expect(fileTree.files).toEqual(
      ["index.html", "other_file.html"]
    )
  })
  it('directories are added correctly', () => {
    expect(Object.keys(fileTree.directories)).toEqual(
      ['here', 'another']
    )
    expect(Object.keys(fileTree.directories.another.directories)).toEqual([])
    expect(Object.keys(fileTree.directories.here.directories)).toEqual(['it'])
    expect(Object.keys(fileTree.directories.here.directories.it.directories)).toEqual(['is'])
    expect(Object.keys(fileTree.directories.here.directories.it.directories.is.directories)).toEqual([])
  })
  it('subdirectories contain the correct files', () => {
    expect(fileTree.directories['another'].files).toEqual(
      ['file.html', 'index.html']
    )
    expect(fileTree.directories['here'].files).toEqual([])
    expect(fileTree.directories['here'].directories['it'].files).toEqual(['index.html'])
    expect(fileTree.directories['here'].directories['it'].directories['is'].files).toEqual(['index.html', 'file.html'])
  })
})

describe('toFileTree with full file paths', () => {
  const fullPathFileTree = toFileTree(paths, true)
  it('top level files are in in top level files', () => {
    expect(fullPathFileTree.files).toEqual(
      ["index.html", "other_file.html"]
    )
  })
  it('directories are added correctly', () => {
    expect(Object.keys(fullPathFileTree.directories)).toEqual(
      ['here', 'another']
    )
    expect(Object.keys(fullPathFileTree.directories.another.directories)).toEqual([])
    expect(Object.keys(fullPathFileTree.directories.here.directories)).toEqual(['it'])
    expect(Object.keys(fullPathFileTree.directories.here.directories.it.directories)).toEqual(['is'])
    expect(Object.keys(fullPathFileTree.directories.here.directories.it.directories.is.directories)).toEqual([])
  })
  it('subdirectories contain the correct files', () => {
    expect(fullPathFileTree.directories['another'].files).toEqual(
      ['another/file.html', 'another/index.html']
    )
    expect(fullPathFileTree.directories['here'].files).toEqual([])
    expect(fullPathFileTree.directories['here'].directories['it'].files).toEqual(['here/it/index.html'])
    expect(fullPathFileTree.directories['here'].directories['it'].directories['is'].files).toEqual(['here/it/is/index.html', 'here/it/is/file.html'])
  })
})

describe('fileTreeToTreeBeard', () => {
  const fileTree = toFileTree(paths, false)
  const treeBeard = fileTreeToTreeBeard(fileTree)
  it('gets the correct amount of values', () => {
    expect(treeBeard.length).toEqual(4)
  })
  it('has correct top level names', () => {
    expect(treeBeard.map(item => item.name)).toEqual(['index.html', 'other_file.html', 'here', 'another'])
  })
})


describe('toTreeBeard', () => {
  const treeBeard = toTreeBeard(paths, false)
  it('gets the correct amount of values', () => {
    expect(treeBeard.length).toEqual(4)
  })
  it('has correct top level names', () => {
    expect(treeBeard.map(item => item.name)).toEqual(['index.html', 'other_file.html', 'here', 'another'])
  })
})

describe('toTreeBeard fullpath', () => {
  const treeBeard = toTreeBeard(paths, true)
  it('gets the correct amount of values', () => {
    expect(treeBeard.length).toEqual(4)
  })
  it('has correct top level names', () => {
    expect(treeBeard.map(item => item.name)).toEqual(['index.html', 'other_file.html', 'here', 'another'])
  })
})
