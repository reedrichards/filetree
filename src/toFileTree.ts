
interface FileTree {
  files: Array<string>,
  directories?: any,
}

function treeFactory(): FileTree {
  return {
    files: [],
    directories: {}
  }
}

let tree: FileTree = {
  files: [],
  directories: {}
};

/** reduceFunc reduces the last directory of a file path to a node in the `FileTree`
 * @param lastDir string representing the last directory in a file path
 *
 * checks to see if the directory exists and if not creates one using `treeFactory`
 */
const reduceFunc = (acc: FileTree, lastDir: string) => {
  acc.directories[lastDir] = acc.directories[lastDir] || treeFactory()
  return acc.directories[lastDir]
}

/** justFileNameMap maps the list of paths passed to `toFileTree`
 * @param path string representing the path to a file
 *
 * takes the last directory of the filePath and the file,
 * reducing the last directory into the `FileTree` and then
 * adding the file to the `files` key of the FileTree's node
 */
const justFileNameMap = (path: string) => {
  let lastDir = path.split("/").slice(0, -1)
  const file = path.split("/")[path.split("/").length - 1]
  return lastDir.reduce(reduceFunc, tree).files.push(file)
}

/** fullPathFileMap maps the list of paths passed to `toFileTree`
 * @param path string representing the path to a file
 *
 * the same as justFileNameMap but adds the full path to the file to the
 * Filetree.files rather than just the file name
 */
const fullPathFileMap = (path: string) => {
  let lastDir = path.split("/").slice(0, -1)
  return lastDir.reduce(reduceFunc, tree).files.push(path)
}

/** toFileTree takes a list of file paths and builds a FileTree structure
 * @param paths Array of filepaths to be added to filetree
 *
 * @param fullPathFiles Boolean default's to `false`. setting to true sets the
 * entries on FileTree.files to the full path of a file rather than just the
 * filename
 *
 * Input: ['index.html', 'content/index.html', 'content/leaf/index.html']
 *
 * Output = {
 *  files: ['index.html'],
 *  directories: {
 *    'content': {
 *      files: ['index.html'],
 *      directories: {
 *        'leaf': {
 *          files: ['index.html']}
 *          directories: {}
 *         }
 *      }
 *    }
 * }
 */
const toFileTree = (paths: Array<string>, fullPathFiles: Boolean = false): FileTree => {
  fullPathFiles ? paths.map(fullPathFileMap) : paths.map(justFileNameMap)
  // make sure we return a new object every time and reset the base tree
  const treeReturn = { ...tree }
  tree = treeFactory()
  return treeReturn
}

interface TreeBeard {
  name: string,
  children?: any,
}

const mapFilesToNames = (file: string) => { return { 'name': file } }

export const fileTreeToTreeBeard = (fileTree: FileTree): Array<TreeBeard> => {
  let treeBeard: Array<TreeBeard> = fileTree.files.map(mapFilesToNames)
  if (Object.keys(fileTree.directories).length > 0) {
    Object.keys(fileTree.directories).map((key: string) => {
      treeBeard.push({
        'name': key,
        children: fileTreeToTreeBeard(fileTree.directories[key])
      })
    })
  }
  return treeBeard
}

export const toTreeBeard = (paths: Array<string>, fullPathFiles: Boolean = false): Array<TreeBeard> => {
  return fileTreeToTreeBeard(toFileTree(paths, fullPathFiles))
}

export default toFileTree
