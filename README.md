ToFileTree is a package that turns a flat list of file paths into a directory structure.

![npm](https://img.shields.io/npm/v/to-file-tree)
![npm](https://img.shields.io/npm/dt/to-file-tree)
![build](https://gitlab.com/reedrichards/filetree/badges/master/pipeline.svg)

[Documentation](https://reedrichards.gitlab.io/filetree/)

[repo](https://gitlab.com/reedrichards/filetree)

usage:

```javascript
import toFileTree  from 'toFileTree'

const files = ['index.html', 'content/index.html', 'content/leaf/index.html']
const fileTree = toFileTee(files)
```

`fileTree` will have a value of
```javascript
{
    files: ['index.html'],
    directories: {
        'content': {
            files: ['index.html'],
            directories: {
                'leaf': {
                   files: ['index.html']
                }
            }
        }
    }
}
```




## Debugger config for vscode

```javascript
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Debug Jest Tests",
            "type": "node",
            "request": "launch",
            "runtimeArgs": ["--inspect-brk", "${workspaceRoot}/node_modules/.bin/jest", "--runInBand"],
            "console": "integratedTerminal",
            "internalConsoleOptions": "neverOpen"
        }
    ]
}
```
